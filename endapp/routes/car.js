var express = require('express');
var router = express.Router();
const {connection} = require('../util/MySQL')

router.get('/apartment', function (req, res, next) {
    var sql = "SELECT *\n" +
        "from divisions"
    connection.query(sql, function (err, result) {
        if (err) {
            console.log('select error!!', err.message)
            return;
        }
        var dataString = JSON.stringify(result)
        // console.log(dataString)
        res.send(dataString);
    })
});

router.get('/cars', function (req, res, next) {
    // req.query['return_time']
    var sql = `SELECT id,car_info
                FROM car_nanfang
                where bus_division=${req.query['apartment_id']}`
    connection.query(sql, function (err, result) {
        if (err) {
            console.log('select error!!', err.message)
            return;
        }
        var dataString = JSON.stringify(result)
        // console.log(dataString)
        res.send(dataString);
    })
});

router.get('/detail', function (req, res, next) {
    var sql = `SELECT order_id,sum(return_quantity) as sum,gname,code,psn,unit,return_type,car_info,car_num,deliveryman_name,deliveryman_tel,chassis,cargo,user.name,DATE(sales_return.return_time) as return_time
            from sales_return,goods,car_nanfang,user
            where sales_return.order_id=goods.id and sales_return.car_id=car_nanfang.id and sales_return.storeman_id=user.id and sales_return.car_id=${req.query['car_id']} and 
            DATE(sales_return.return_time) = (SELECT DATE(return_time) from sales_return where car_id=${req.query['car_id']} ORDER BY return_time desc LIMIT 1)
            GROUP BY order_id,gname,code,psn,unit,car_info,car_num,deliveryman_name,deliveryman_tel,chassis,cargo
            `
    connection.query(sql, function (err, result) {
        if (err) {
            console.log('select error!!', err)
            return;
        }
        var dataString = JSON.stringify(result)
        // console.log(dataString)
        res.send(dataString);
    })
});

router.get('/change', function (req, res, next) {
    var sql
    if (!Number(req.query['chassis'])) {
        sql = `update car_nanfang
            set chassis=${req.query['count']}
            where id=${req.query['car_id']}`
    } else {
        sql = `update car_nanfang
            set cargo=${req.query['count']}
            where id=${req.query['car_id']}`
    }
    connection.query(sql, function (err, result) {
        if (err) {
            console.log('select error!!', err.message)
            return;
        }
        var dataString = JSON.stringify(result)
        // console.log(dataString)
        res.send(dataString);
    })
});

router.get('/compare', function (req, res, next) {
    var sql = `SELECT chassis,cargo,real_chassis,real_cargo
        from car_nanfang
        where id=${req.query['car_id']}`
    connection.query(sql, function (err, result) {
        if (err) {
            console.log('select error!!', err.message)
            return;
        }
        var dataString = JSON.stringify(result)
        // console.log(dataString)
        res.send(dataString);
    })
});

module.exports = router;
// 相同点
// Express 的 res.end() 和 res.send() 方法的相同点：
// 1. 二者最终都是回归到  http.ServerResponse.Use 的 response.end() 方法。
// 2. 二者都会结束当前响应流程。
// 不同点
// Express 的 res.end() 和 res.send() 方法的不同点：
// 1. 前者只能发送 string 或者 Buffer 类型，后者可以发送任何类型数据。
// 2. 从语义来看，前者更适合没有任何响应数据的场景，而后者更适合于存在响应数据的场景。