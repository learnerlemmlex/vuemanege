import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
const actions = {
    getApartment(context, value) {
        context.commit('GETAPARTMENT', value)
    },
    getCars(context, value) {
        context.commit('GETCARS', value)
    },
    getCarId(context,value){
        context.commit('GETCARID',value)
    },
    getInformation(context, value) {
        context.commit('GETINFORMATION', value)
    },
    updateData(context, value) {
        context.commit('UPDATEDATA', value)
    }
}
const mutations = {
    GETAPARTMENT(state, value) {
        state.apartment = value
    },
    GETCARS(state, value) {
        state.cars = value
    },
    GETCARID(state,value){
        state.carid = value
    },
    GETINFORMATION(state, value) {
        let count = 0
        let sort = 0
        value.forEach((item) => {
            count += item.sum
            sort++
        })
        state.information = {
            car_info: value[0]['car_info'],
            count,
            sort,
            chassis: value[0]['chassis'],
            cargo: value[0]['cargo'],
            name: value[0]['deliveryman_name'],
            phone: value[0]['deliveryman_tel'],
            time: value[0]['return_time'],
            operator: value[0]['name'],
        }
    },
    UPDATEDATA(state, value) {
        if (!value['status']) {
            state.information['chassis'] = value['number']
        } else {
            state.information['cargo'] = value['number']
        }
    }
}
const state = {
    apartment: [],
    cars: [],
    information: {},
    carid:0
}
export default new Vuex.Store({
    actions,
    mutations,
    state
})