const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave:false,
  devServer:{
    proxy:{
      'api1':{
        target:'http://localhost:8082/car',
        changeOrigin:true,
        pathRewrite:{'^/api1':''}
      }
    }
  }
})
